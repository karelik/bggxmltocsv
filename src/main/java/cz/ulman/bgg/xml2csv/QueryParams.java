package cz.ulman.bgg.xml2csv;

import io.mikael.urlbuilder.UrlBuilder;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Object represents data for web server query to obtain response and grab data of interest from it.
 */
class QueryParams {
	private static final Pattern LOOP_ELEMENT_PATTERN = Pattern.compile("(\\w+)\\[]");
	private static final Pattern VAR_PATTERN = Pattern.compile("var(\\d{1,2})");
	private static final String SELECTORS_DELIMITER = ",";

	private final String[] selectors;
	private final String url;
	private String loopElement;

	/**
	 * Parses params:
	 *  url - url of web service, required
	 *  xml.selectors - selectors delimited by comma, required
	 *  csv.out - directory of csv results, required
	 *
	 *  xml.out - directory of xml response/s to query/ies, not required, if not present query response is not saved
	 *  xml.prefix - common part of all xpath queries, not required
	 *  csv.delimiter - delimiter of csv records, not required, default ';'
	 *  var0 or var1 etc. - variables, you can add them to url by {0},{1} etc.
	 *
	 *  you can use symbol [] to mark iteration tag in url or xml.prefix
	 * @param properties
	 */
	QueryParams(Properties properties) {

		MessageFormat mf = null;
		List<Object> objectList = new ArrayList<>();
		String prefix = "";
		String[] selectorsStr = null;

		for (Map.Entry<Object, Object> entry : properties.entrySet()) {
			String entryKey = (String) entry.getKey();
			String entryValue = (String) entry.getValue();
            switch (entryKey) {
                case "url" -> mf = new MessageFormat(entryValue);
                case "xml.prefix" -> prefix = entryValue;
                case "xml.selectors" -> selectorsStr = entryValue.split(SELECTORS_DELIMITER);
                default -> {
                    // variables
                    Matcher m = VAR_PATTERN.matcher(entryKey);
                    if (m.find()) {
                        int index = Integer.parseInt(m.group(1));
                        objectList.add(index, UrlBuilder.fromString(entryValue));
                    }
                }
            }
		}
		assert mf != null && selectorsStr != null;

		this.selectors = new String[selectorsStr.length];
		int i = 0;
		for (String selStr : selectorsStr) {
			String selector = prefix + selStr;
			if (this.loopElement == null) {
				Matcher m = LOOP_ELEMENT_PATTERN.matcher(selector);
				if (m.find()) {
					this.loopElement = m.group(1);
				}
			}
			this.selectors[i++] = selector;
		}
		this.url = mf.format(objectList.toArray());
	}

	String[] getSelectors() {
		return this.selectors;
	}

	String getUrl() {
		return this.url;
	}

	String getLoopElement() {
		return this.loopElement;
	}
}
