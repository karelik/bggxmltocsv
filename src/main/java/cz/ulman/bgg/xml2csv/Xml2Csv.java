package cz.ulman.bgg.xml2csv;

import com.opencsv.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

class Xml2Csv {
	private static final Logger log = Logger.getLogger(Xml2Csv.class.getName());

	private final ProgramParams programParams;
	private final QueryParams queryParams;
	private static final String[] HELP_ROWS = {"Usage: xml2csv CONFIG.properties [CSV.csv] [-D';'] [XML.xml]"};

	Xml2Csv(ProgramParams programParams, QueryParams queryParams) {
		this.programParams = programParams;
		this.queryParams = queryParams;
	}

	/**
	 * Core of app.
	 *
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	private void run() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, URISyntaxException {
		Document responseXml = askUrl();
		writeXml(responseXml);
		writeCsv(responseXml);
	}

	/**
	 * Writes csv file with results of XPath query.
	 *
	 * @param responseXml
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	private void writeCsv(Document responseXml) throws IOException, XPathExpressionException {
		Writer innerWriter = programParams.isCommandLineMode() ? new OutputStreamWriter(System.out) : new FileWriter(programParams.getCsvOut());
		CSVWriter writer = new CSVWriter(innerWriter, programParams.getCsvDelimiter(), ICSVWriter.NO_QUOTE_CHARACTER, ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END);
		int games = responseXml.getElementsByTagName(queryParams.getLoopElement()).getLength();
		List<String[]> lines = getXPathResults(responseXml, games);
		writer.writeAll(lines);
		writer.flush();
		writer.close();
		if (!programParams.isCommandLineMode()) log.info("CSV file saved!");
	}

	/**
	 * Collects results of XPath queries. There is trick with "[]" symbol which marks iteration tag. It will iterate this tag and query selector. It prepares csv lines to result.
	 *
	 * @param responseXml
	 * @param games
	 * @return
	 * @throws XPathExpressionException
	 */
	private List<String[]> getXPathResults(Document responseXml, int games) throws XPathExpressionException {
		List<String[]> lines = new ArrayList<>();
		XPathFactory xPathFactory = XPathFactory.newInstance();
		XPath xpath = xPathFactory.newXPath();
		for (int i = 0; i < games; i++) {
			List<String> line = new ArrayList<>();
			for (String selector : queryParams.getSelectors()) {
				String specialSelector = selector.replace("[]", "[" + (i + 1) + "]");
				XPathExpression xPathExpression = xpath.compile(specialSelector);
				Object result = xPathExpression.evaluate(responseXml, XPathConstants.NODESET);
				if (result != null && ((NodeList) result).item(0) != null) {
					String val = ((NodeList) result).item(0).getTextContent();
					if (val != null) {
						line.add(val);
					}
				}
			}
			lines.add(line.toArray(new String[0]));
		}
		return lines;
	}

	/**
	 * Backups source XML to file.
	 *
	 * @param sourceXml
	 */
	private void writeXml(Document sourceXml) {
		if (programParams.isXmlSaveEnabled()) {
			try {
				// Use a Transformer for output
				TransformerFactory tFactory = TransformerFactory.newInstance();
				Transformer transformer = tFactory.newTransformer();

				DOMSource source = new DOMSource(sourceXml);
				StreamResult result = new StreamResult(new FileOutputStream(programParams.getXmlOut()));
				transformer.transform(source, result);
				log.info("XML response saved!");
			} catch (TransformerException | FileNotFoundException e) {
				log.log(Level.SEVERE, "Backup of source XML failed!", e);
			}
		}
	}

	/**
	 * Queries web service and get result XML as Document
	 *
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private Document askUrl() throws IOException, ParserConfigurationException, SAXException, URISyntaxException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		long begin = System.currentTimeMillis();
		Document doc = db.parse(new URI(queryParams.getUrl()).toURL().openStream());
		long end = System.currentTimeMillis();
		long durationMillis = end - begin;
		log.info("URL: " + queryParams.getUrl());
		log.info("Query completed! (elapsed " + durationMillis + " ms)");
		return doc;
	}

	/**
	 * Process inputs from config.properties file and run app.
	 *
	 * @param args
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws XPathExpressionException
	 */
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException, URISyntaxException {
		ProgramParams programParams = new ProgramParams(args);
		if (!programParams.isOk()) showHelp(programParams.getErrorMessageList());
		else {
			Properties properties = new Properties();
			properties.load(new FileReader(programParams.getConfig()));
			QueryParams queryParams = new QueryParams(properties);
			Xml2Csv xml2Csv = new Xml2Csv(programParams, queryParams);
			xml2Csv.run();
		}
	}

	private static void showHelp(List<String> errorMessageList) {
		errorMessageList.forEach(System.out::println);
		for (String row : HELP_ROWS){
			System.out.println(row);
		}
	}
}
