package cz.ulman.bgg.xml2csv;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Object representation of input (command line) data.
 */
class ProgramParams {
	private File config, csvOut, xmlOut = null;
	private char csvDelimiter = ';';
	private final List<String> errorMessageList = new ArrayList<>();

	private static final Pattern DELIMITER_PATTERN = Pattern.compile("-D'(.*)'");

	ProgramParams(String[] args) {
		String propertiesFilename = null;
		String csvFilename = null;
		String xmlFilename = null;
		Character delimiter = null;
		for (String param : args) {
			if (param.endsWith(".properties") && propertiesFilename == null){
				propertiesFilename = param;
				continue;
			}
			if (param.endsWith(".xml") && xmlFilename == null){
				xmlFilename = param;
				continue;
			}
			if (param.endsWith(".csv") && csvFilename == null){
				csvFilename = param;
				continue;
			}
			if (delimiter == null) {
				Matcher m = DELIMITER_PATTERN.matcher(param);
				if (m.matches()) {
					String delimiterStr = m.group(1);
					if (delimiterStr.length() == 1) delimiter = delimiterStr.charAt(0);
					else errorMessageList.add("CSV delimiter problem!");
				}
			}
		}
		if (propertiesFilename != null) {
			config = new File(propertiesFilename);
			if (!config.isFile()) errorMessageList.add("Config file error!");
		} else errorMessageList.add("Config file not defined!");
		if (csvFilename != null) {
			csvOut = new File(csvFilename);
			if (csvOut.isFile() && !csvOut.canWrite()) errorMessageList.add("Can't write to CSV file!");
		}
		if (xmlFilename != null) {
			xmlOut = new File(xmlFilename);
			if (xmlOut.isFile() && !xmlOut.canWrite()) errorMessageList.add("Can't write to XML file!");
		}
		if (delimiter != null) csvDelimiter = delimiter;
	}

	/**
	 * If inputs are ok, is possible to run program.
	 *
	 * @return
	 */
	boolean isOk() {
		return errorMessageList.isEmpty();
	}

	/**
	 * Save XML response from server to file?
	 *
	 * @return
	 */
	boolean isXmlSaveEnabled() {
		return xmlOut != null;
	}

	/**
	 * If output is to commandline (not present csv output file).
	 *
	 * @return
	 */
	boolean isCommandLineMode() {
		return csvOut == null;
	}

	File getConfig() {
		return this.config;
	}

	File getCsvOut() {
		return this.csvOut;
	}

	File getXmlOut() {
		return this.xmlOut;
	}

	char getCsvDelimiter() {
		return this.csvDelimiter;
	}

	List<String> getErrorMessageList() {
		return this.errorMessageList;
	}
}
