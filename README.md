# BGG XML to CSV #
## Motivation
You are board game geek and want to create XLS sheet with list of games with basic data for your gaming event or collection etc. Rewriting of all data for every game is time consuming. There is simpler solution.

The most famous board game portal is [BoardGameGeek](http://www.boardgamegeek.com). It has huge database and every game there has unique number identifier *BGG item ID*. Portal offers XML API to gather information from portal so you can create list of game identifiers and use it to gather data you want to. Where is BGG Item ID located is marked by yellow rectangle in this screenshot.
![BGG screenshot](http://www.deskos.cz/images/102.png)

## Description
My simple command line program will help you to gather data from BGG API (but it can be **any web uses public XML API**) and transform responses **to CSV file** which is simple way to import to spreadsheet application (Calc, Excel) or database.
You have to have define url (you will need list of BGG IDs), [XPath selectors](http://www.w3schools.com/xsl/xpath_syntax.asp) (optional prefix, variables and mark iteration tag) to simple properties file and run with parameters (config file required, optional xml file, csv file and csv delimiter) and that's all. Program writes to console CSV result or writes file with result.

## Change log ##
Version | Description | JAR |
------- |-------------|-----|
1.00 | Initial release | [BGGXml2Csv-1.0-jar-with-dependencies.jar](https://bitbucket.org/karelik/bggxmltocsv/downloads/BGGXml2Csv-1.0-jar-with-dependencies.jar) |

## Example ##

To load *config.properties*, run query, save response to *response.xml* and collected results save to *games.csv* delimited with commas.
~~~~
BGGXml2Csv config.properties -D',' response.xml games.csv
~~~~

### Run parameters ###

Parameter | Required | Default | Description | Required extension |
:---------|:--------:|:-------:|-------------|:------------------:|
config.properties | YES | NONE | Sets config file that describes details of query and result. | .properties |
-D',' | NO | ; | Sets CSV delimiter (to comma). | |
response.xml | NO | NONE | Turns on save of XML response to file. If you skip this parameter, response is not saved. | .xml |
games.csv | NO | NONE | Sets result file. If you skip this parameter, CSV result is written to screen. | .csv |

**config.properties** file:
~~~~
url=https://www.boardgamegeek.com/xmlapi/boardgame/{0}
var0=320,340,533
xml.prefix=//boardgames/boardgame[]/
xml.selectors=@objectid,name[@primary='true']/text(),boardgamedesigner/text(),boardgamepublisher/text(),minplayers/text(),maxplayers/text(),yearpublished/text()
~~~~
**var0** is definition of variable, **{0}** is where variable is placed. You can use var1 and {1} to another place in url.
**[]** in xml.prefix means that CSV rows are iteration of boardgame tags and data of row consists of selectors results.

And result files are:

**games.csv**
~~~~
"194938","Automobilová soutěž","Tofa Semily","2","4","1990"
"194968","Hrady","Dino","3","5","1990"
"51128","Stavíme Stalinův Pomník","Ladislav Horáček","(Unknown)","0","0","1990"
~~~~
Result. You can see BGG ID, primary name, designer name, publisher name, players range (min,max) and publish year. All delimited by commas.

**response.xml**
~~~~
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<boardgames termsofuse="http://boardgamegeek.com/xmlapi/termsofuse">
<boardgame objectid="194938">
<yearpublished>1990</yearpublished>
<minplayers>2</minplayers>
<maxplayers>4</maxplayers>
<playingtime>0</playingtime>
<minplaytime>0</minplaytime>
<maxplaytime>0</maxplaytime>
<age>7</age>
...
~~~~
Whole response of BGG API. You can use it later in another program etc.